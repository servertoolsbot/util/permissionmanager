CREATE TABLE users(
	id varchar(255),
	name varchar(255)
);

CREATE TABLE user_permissions(
	id varchar(255),
	name varchar(255),
	value varchar(255)
);

CREATE TABLE user_parents(
	id varchar(255),
	parent varchar(255)
);

CREATE TABLE groups(
	id varchar(255),
	name varchar(255),
	weight varchar(255)
);

CREATE TABLE group_permissions(
	id varchar(255),
	name varchar(255),
	value varchar(255)
);

CREATE TABLE group_parents(
	id varchar(255),
	parent varchar(255)
);
