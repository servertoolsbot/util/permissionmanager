package com.gitlab.servertoolsbot.test;

import java.util.ArrayList;
import java.util.List;

class Util {
    public static List<String> inputToList(String input) {
        List<String> paramBuilder = new ArrayList<>();
        for (String inputPart : input.split(" ")) {
            paramBuilder.add(inputPart);
        }
        return paramBuilder;
    }
}
