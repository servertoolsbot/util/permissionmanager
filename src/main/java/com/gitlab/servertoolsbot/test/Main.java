package com.gitlab.servertoolsbot.test;

import com.gitlab.servertoolsbot.util.permissionmanager.*;
import com.gitlab.servertoolsbot.util.permissionmanager.configuration.PermissionStorageType;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Main {
    private static List<String> globalParam = new ArrayList<>();
    public static PermissionManager permissionManager;

    public static String getParam(Integer index) {
        if (!(index < globalParam.size())) return null;
        return globalParam.get(index);
    }

    public static void main(String[] args) {
        System.out.println("Starting!");
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            onCommand(sc.nextLine());
        }
        sc.close();
    }

    public static void onCommand(String input) {
        List<String> param = Util.inputToList(input);
        globalParam = param;
        setupManager();
        permissionManager.start();
        GroupManager gm = permissionManager.getGroupManager();
        UserManager um = permissionManager.getUserManager();
        if (getParam(0).equalsIgnoreCase("testdb")) {
            User u = new User("supernewuser");
            u.addPermission("test.perm", false);
            um.saveUser(u);
        }
        if (getParam(0).equalsIgnoreCase("addGroup")) {
            gm.saveGroup(new Group(getParam(1)));
        }
        if (getParam(0).equalsIgnoreCase("addUser")) {
            um.saveUser(new User(getParam(1)));
        }
        if (getParam(0).equalsIgnoreCase("deleteGroup")) {
             gm.deleteGroup(new Group(getParam(1)));
        }
        if (getParam(0).equalsIgnoreCase("getAllGroups")) {
        }
        if (getParam(0).equalsIgnoreCase("weightTest")) {
            User user = um.getUser("test");
            System.out.println(user.hasPermission("test.perm"));
        }
        if (getParam(0).equalsIgnoreCase("stressTest")) {
            long time = System.currentTimeMillis();
            Group main = gm.getGroup("main");

            long permissionSetupTime = System.currentTimeMillis();
            for (int i = 0; i < 100; i++) {
                Group g = new Group(String.valueOf(i));
                for (int i1 = 0; i1 < 100; i1++) {
                    g.addPermission(String.valueOf(i1), true);
                }
                gm.saveGroup(g);
                main.addParent(g);
            }
            gm.saveGroup(main);
            System.out.println("Finished permissionSetup in " + (System.currentTimeMillis() - permissionSetupTime) + "ms");
            long permissionReadTime = System.currentTimeMillis();
            for (Group group : gm.getGroup("main").getParents()) {
                for (int i = 0; i < 100; i++) {
                    System.out.println(group.hasPermission(String.valueOf(i)));
                }
            }
            System.out.println("Finished permissionReadTime in " + (System.currentTimeMillis() - permissionReadTime) + "ms");
            System.out.println("Finished total in " + (System.currentTimeMillis() - time) + "ms");
        }
    }

    public static void setupManager() {
        PermissionManager manager = new PermissionManager();
        PermissionStorageType type = PermissionStorageType.MARIADB;
        //type.setDir(new File("permissions"));
        type.setDatabase("wikidevbot");
        type.setHost("localhost");
        type.setPassword("wikidevbotpassword");
        type.setUser("wikidevbot");
        type.setGroupTable("groups");
        type.setGroupPermissionTable("group_permissions");
        type.setGroupParentTable("group_parents");
        type.setUserTable("users");
        type.setUserParentTable("user_parents");
        type.setUserPermissionTable("user_permissions");
        manager.setStorageType(type);
        permissionManager = manager;
    }
}
