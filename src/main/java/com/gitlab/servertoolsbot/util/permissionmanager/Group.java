package com.gitlab.servertoolsbot.util.permissionmanager;

public class Group extends PermissionObject {
    private int weight = 0;

    public Group() {

    }
    public Group(String name) {
        setName(name);
    }

    public int getWeight() {
        return this.weight;
    }
    public Group setWeight(int weight) {
        this.weight = weight;
        return this;
    }
}
