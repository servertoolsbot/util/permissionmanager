package com.gitlab.servertoolsbot.util.permissionmanager.util.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;

public class GetKey {
    public static JsonElement get(File file, String key) {
        return get(GetObject.get(file), key);
    }

    public static JsonElement get(JsonObject obj, String key) {
        return obj.get("key");
    }
}
