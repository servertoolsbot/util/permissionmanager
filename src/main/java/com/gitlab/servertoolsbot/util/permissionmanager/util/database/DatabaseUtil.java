package com.gitlab.servertoolsbot.util.permissionmanager.util.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseUtil {
    private Connection connection;

    public DatabaseUtil(Connection connection) {
        this.connection = connection;
    }

    public ResultSet getResultFromQuery(String query) {
        try {
            return connection.createStatement().executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
