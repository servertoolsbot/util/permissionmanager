package com.gitlab.servertoolsbot.util.permissionmanager.util;

import com.gitlab.servertoolsbot.util.permissionmanager.Tristate;

public class TristateUtils {
    public static boolean asBoolean(Tristate value) {
        if (value == Tristate.TRUE) return true;
        if (value == Tristate.FALSE || value == Tristate.UNDEFINED) return false;
        return false;
    }

    public static Tristate toTristate(boolean value) {
        if (value) return Tristate.TRUE;
        if (!value) return Tristate.FALSE;
        return Tristate.UNDEFINED;
    }
}
