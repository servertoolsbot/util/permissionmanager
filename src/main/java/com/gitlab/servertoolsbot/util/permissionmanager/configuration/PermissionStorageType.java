package com.gitlab.servertoolsbot.util.permissionmanager.configuration;

import java.io.File;

/**
 * Possible PermissionStorageTypes
 */
public enum PermissionStorageType {

    /**
     * Uses JSON files as the storage
     */
    JSON,

    /**
     * Uses a MariaDB database as the storage
     */
    MARIADB,

    /**
     * Default, needs to be changed using {@link com.gitlab.servertoolsbot.util.permissionmanager.PermissionManager#setStorageType(PermissionStorageType)}
     */
    UNDEFINED;


    /**
     * JSON related settings
     */
    private File dir = new File("permissions");

    /**
     * MariaDB related settings
     */
    private String user;
    private String password;
    private String database;
    private String groupTable;
    private String groupParentTable;
    private String groupPermissionTable;
    private String userTable;
    private String userParentTable;
    private String userPermissionTable;
    private String host;

    /**
     * Setup of Json
     * @param dir the folder used to get data as JSON files
     */
    public void setupJson(File dir) {
        this.setDir(dir);
    }
    public void setDir(File dir) {
        this.dir = dir;
    }
    public File getDir() {
        return this.dir;
    }

    /**
     * Setup of MariaDB
     * @param user       the user to the access the database
     * @param password   the password of the user
     * @param database   the database name
     * @param groupTable the group table name
     * @param groupParentTable      the group parent table name
     * @param groupPermissionTable  the group permission table name
     * @param userTable   the user table name
     * @param userParentTable       the user parent table name
     * @param userPermissionTable   the user permission table name
     * @param host       the hostname or ip address
     */
    public void setupMariaDB(String user, String password, String database, String groupTable, String groupParentTable, String groupPermissionTable, String userTable, String userParentTable, String userPermissionTable, String host) {
        this.setUser(user);
        this.setPassword(password);
        this.setDatabase(database);
        this.setGroupTable(groupTable);
        this.setGroupParentTable(groupParentTable);
        this.setGroupPermissionTable(groupPermissionTable);
        this.setUserTable(userTable);
        this.setUserParentTable(userParentTable);
        this.setUserPermissionTable(userPermissionTable);
        this.setHost(host);
    }
    public void setUser(String user) {
        this.user = user;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setDatabase(String database) {
        this.database = database;
    }
    public void setGroupTable(String groupTable) {
        this.groupTable = groupTable;
    }
    public void setGroupParentTable(String groupParentTable) {
        this.groupParentTable = groupParentTable;
    }
    public void setGroupPermissionTable(String groupPermissionTable) {
        this.groupPermissionTable = groupPermissionTable;
    }
    public void setUserTable(String userTable) {
        this.userTable = userTable;
    }
    public void setUserParentTable(String userParentTable) {
        this.userParentTable = userParentTable;
    }
    public void setUserPermissionTable(String userPermissionTable) {
        this.userPermissionTable = userPermissionTable;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getUser() {
        return this.user;
    }
    public String getPassword() {
        return this.password;
    }
    public String getDatabase() {
        return this.database;
    }
    public String getGroupTable() {
        return this.groupTable;
    }
    public String getGroupParentTable() {
        return this.groupParentTable;
    }
    public String getGroupPermissionTable() {
        return this.groupPermissionTable;
    }
    public String getUserTable() {
        return this.userTable;
    }
    public String getUserParentTable() {
        return this.userParentTable;
    }
    public String getUserPermissionTable() {
        return this.userPermissionTable;
    }
    public String getHost() {
        return this.host;
    }

}
