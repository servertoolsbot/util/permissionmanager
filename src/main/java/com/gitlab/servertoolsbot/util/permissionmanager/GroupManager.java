package com.gitlab.servertoolsbot.util.permissionmanager;

import java.util.List;

public class GroupManager {
    GroupManager() {

    }

    public GroupManager saveGroup(Group group) {
        StorageManager.saveGroup(group);
        return this;
    }
    public GroupManager saveGroups(Group... groups) {
        for (Group group : groups) this.saveGroup(group);
        return this;
    }

    public Group getGroup(String name) {
        return StorageManager.getGroup(name);
    }

    public GroupManager deleteGroup(Group group) {
        StorageManager.deleteGroup(group);
        return this;
    }
    public GroupManager deleteGroup(String name) {
        this.deleteGroup(getGroup(name));
        return this;
    }
    public GroupManager deleteGroups(Group... groups) {
        for (Group group : groups) this.deleteGroup(group);
        return this;
    }
    public GroupManager deleteGroups(String... groups) {
        for (String group : groups) this.deleteGroup(group);
        return this;
    }

    public List<Group> getAllGroups() {
        return StorageManager.getAllGroups();
    }
}
