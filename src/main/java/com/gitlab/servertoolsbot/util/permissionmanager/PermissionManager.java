package com.gitlab.servertoolsbot.util.permissionmanager;

import com.gitlab.servertoolsbot.util.permissionmanager.configuration.PermissionStorageType;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PermissionManager {
    public static PermissionManager instance = null;

    private PermissionStorageType storageType = PermissionStorageType.UNDEFINED;

    private Connection connection;

    public PermissionManager() {

    }

    PermissionStorageType getStorageType() {
        this.checkSettings();
        return storageType;
    }


    public void start() {
        this.checkSettings();
        this.loadData();
        instance = this;
    }
    public void stop() {
        instance = null;
    }

    public GroupManager getGroupManager() {
        return new GroupManager();
    }
    public UserManager getUserManager() {
        return new UserManager();
    }


    private void checkSettings() {
        if (this.storageType == PermissionStorageType.UNDEFINED) throw new IllegalStateException("PermissionStorageType hasn't been set, aborting process");
        if (this.storageType.getDir() == null && this.storageType == PermissionStorageType.JSON) throw new IllegalStateException("Dir needs to be set when using PermissionStorageType.JSON");
        if (this.storageType == PermissionStorageType.MARIADB) {
            if (
                    this.storageType.getDatabase() == null ||
                    this.storageType.getHost() == null ||
                    this.storageType.getUser() == null ||
                    this.storageType.getPassword() == null ||
                    this.storageType.getUserTable() == null ||
                    this.storageType.getGroupTable() == null
            )
                throw new IllegalStateException("Not all database related information has been set, please use the method PermissionStorageType#setupMariaDB");
        }
    }
    private void loadData() {
        if (this.storageType == PermissionStorageType.JSON && !this.storageType.getDir().exists()) this.createDirs();
        if (this.storageType == PermissionStorageType.MARIADB && this.connection == null) this.createConnection();
    }

    private void createDirs() {
        JsonObject obj = new JsonObject();
        File baseDir = this.storageType.getDir();
        baseDir.mkdirs();
        File[] toCreate = new File[]{new File(baseDir + "/users.json"), new File(baseDir + "/groups.json")};
        for (File file : toCreate) {
            try (FileWriter writer = new FileWriter(file)) {
                writer.append(new GsonBuilder().setPrettyPrinting().create().toJson(obj));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void createConnection() {
        try {
            this.connection = DriverManager.getConnection(
                    "jdbc:mariadb://" + this.storageType.getHost() + ":3306/"
                    + storageType.getDatabase()
                    + "?user=" + storageType.getUser()
                    + "&password=" + storageType.getPassword()
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setStorageType(PermissionStorageType type) {
        this.storageType = type;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public static PermissionManager getInstance() {
        return instance;
    }
}
