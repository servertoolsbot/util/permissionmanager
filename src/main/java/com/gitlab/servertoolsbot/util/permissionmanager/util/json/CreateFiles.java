package com.gitlab.servertoolsbot.util.permissionmanager.util.json;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateFiles {
    private static void generate(FileWriter writer) throws IOException {
        writer.flush();
    }

    public static void generateWithJson(File file, JsonObject obj) {
        try (FileWriter writer = new FileWriter(file)) {
            writer.append(new GsonBuilder().setPrettyPrinting().create().toJson(obj));
            generate(writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void generate(File file) {
        try (FileWriter writer = new FileWriter(file)) {
            generate(writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
