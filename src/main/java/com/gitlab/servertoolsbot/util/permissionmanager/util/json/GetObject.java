package com.gitlab.servertoolsbot.util.permissionmanager.util.json;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class GetObject {
    public static JsonObject get(File file) {
        try {
            return (JsonObject) new JsonParser().parse(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return new JsonObject();
    }
}
