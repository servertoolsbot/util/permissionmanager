package com.gitlab.servertoolsbot.util.permissionmanager;

import com.gitlab.servertoolsbot.util.permissionmanager.util.TristateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class PermissionObject {
    private String name;

    private List<Permission> permissions = new ArrayList<>();
    private List<Permission> cachedPermissions = new ArrayList<>();
    private List<Group> parents = new ArrayList<>();
    private List<Group> cachedParents = new ArrayList<>();

    public PermissionObject() {

    }
    public PermissionObject(String name) {
        this.setName(name);
    }

    public PermissionObject setName(String name) {
        this.name = name;
        return this;
    }
    public String getName() {
        return this.name;
    }

    public List<Group> getCachedParents() {
        return this.cachedParents;
    }
    public List<Group> getParents() {
        List<Group> parentList = new ArrayList<>();

        if (!this.getRawParents().isEmpty()) {
            for (Group parent : getRawParents()) {
                parentList.add(parent);
                parentList.addAll(this.parentUtil(parent));
            }
        }

        return sortedParents(parentList);
    }
    public List<Group> getRawParents() {
        return this.sortedParents(this.parents);
    }
    private List<Group> parentUtil(Group parent) {
        List<Group> toReturn = new ArrayList<>();
        if (!parent.getRawParents().isEmpty()) {
            for (Group group0 : parent.getRawParents()) {
                toReturn.add(group0);
                toReturn.addAll(parentUtil(group0));
            }
        }
        return toReturn;
    }
    private List<Group> sortedParents(List<Group> parentList) {
        parentList = parentList.stream().distinct().sorted(Comparator.comparingInt(Group::getWeight)).collect(Collectors.toList());
        Collections.reverse(parentList);
        return parentList;
    }

    public List<Permission> getPermissions() {
        List<Permission> toReturn = new ArrayList<>();
        List<Permission> holder = new ArrayList<>();
        if (this.getParents().size() > 0 && this instanceof Group) {
            List<Group> newParents = this.getParents();
            newParents.add((Group)this);
            this.sortedParents(newParents).forEach(group -> {
                toReturn.addAll(group.getRawPermissions());
            });
            holder.addAll(toReturn);
        } else {
            holder = this.getRawPermissions();
        }
        return holder;
    }
    private List<Group> moveOn(List<Group> groupsToMoveOn, int i) {

        return groupsToMoveOn;
    }
    public List<Permission> getRawPermissions() {
        return this.permissions;
    }

    public PermissionObject addPermission(String name, Tristate value) {
        if (!this.permissions.contains(new Permission(name, value))) {
            this.permissions.add(new Permission(name, value));
        }
        return this;
    }
    public PermissionObject addPermission(String name, boolean value) {
        this.addPermission(name, TristateUtils.toTristate(value));
        return this;
    }
    public PermissionObject addPermission(Permission permission) {
        this.addPermission(permission.getName(), permission.getValue());
        return this;
    }
    public PermissionObject addPermissions(Permission... permissions) {
        this.permissions.clear();
        for (Permission permission : permissions) this.addPermission(permission);
        return this;
    }

    public PermissionObject addParent(Group parent) {
        this.parents.add((Group) parent);
        return this;
    }
    public PermissionObject addParents(Group... parents) {
        for (Group parent : parents) {
            this.addParent(parent);
        }
        return this;
    }
    public PermissionObject setParent(Group group) {
        this.parents.clear();
        this.addParent(group);
        return this;
    }
    public PermissionObject setParents(Group... parents) {
        this.parents.clear();
        this.addParents(parents);
        return this;
    }


    public boolean hasPermission(String name) {
        return TristateUtils.asBoolean(getPermission(name));
    }
    public Tristate getPermission(String name) {
        for (Permission permission : this.getPermissions()) {
            if (this.permissionCheckPassed(name, permission)) return permission.getValue();
        }
        for (Group group : this.getParents()) {
            for (Permission permission : group.getPermissions()) {
                if (this.permissionCheckPassed(name, permission)) return permission.getValue();
            }
        }
        return Tristate.UNDEFINED;
    }

    private boolean permissionCheckPassed(String searched, Permission currentPermission) {
        return currentPermission.getName()
                .equalsIgnoreCase(searched)
                ||
                (
                        currentPermission.isRegexPermission()
                                &&
                                searched.matches(
                                        currentPermission.getName().replace("R=", "")
                                )
                );
    }

    public Group toGroup() {
        Group group = new Group();
        group.addPermissions(this.getPermissions().toArray(new Permission[]{}));
        group.addParents(this.getRawParents().toArray(new Group[]{}));
        group.setName(this.getName());
        return group;
    }
    public User toUser() {
        User user = new User();
        user.addPermissions(this.getPermissions().toArray(new Permission[]{}));
        user.addParents(this.getRawParents().toArray(new Group[]{}));
        user.setName(this.getName());
        return user;
    }
}
