package com.gitlab.servertoolsbot.util.permissionmanager;

public enum Tristate {
    TRUE,
    FALSE,
    UNDEFINED
}
