package com.gitlab.servertoolsbot.util.permissionmanager;

public class Permission {
    private String name;
    private Tristate value = Tristate.UNDEFINED;

    public Permission(String name, Tristate value) {
        this.setName(name);
        this.setValue(value);
    }

    public void setValue(Tristate value) {
        this.value = value;
    }
    public Tristate getValue() {
        return this.value;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

    public boolean isRegexPermission() {
        return name.startsWith("R=");
    }
}
