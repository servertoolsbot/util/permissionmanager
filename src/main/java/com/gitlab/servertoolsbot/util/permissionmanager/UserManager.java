package com.gitlab.servertoolsbot.util.permissionmanager;

import java.util.List;

public class UserManager {
    UserManager() {

    }

    public UserManager saveUser(User user) {
        StorageManager.saveUser(user);
        return this;
    }
    public UserManager saveUsers(User... users) {
        for (User user : users) saveUser(user);
        return this;
    }

    public User getUser(String name) {
        return StorageManager.getUser(name);
    }

    public UserManager deleteUser(User user) {
        StorageManager.deleteUser(user);
        return this;
    }
    public UserManager deleteUser(String name) {
        deleteUser(getUser(name));
        return this;
    }
    public UserManager deleteUsers(User... users) {
        for (User user : users) deleteUser(user);
        return this;
    }
    public UserManager deleteUsers(String... users) {
        for (String user : users) deleteUser(user);
        return this;
    }

    public List<User> getAllUsers() {
        return StorageManager.getAllUsers();
    }
}
