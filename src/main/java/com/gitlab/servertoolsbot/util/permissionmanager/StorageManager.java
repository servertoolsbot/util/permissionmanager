package com.gitlab.servertoolsbot.util.permissionmanager;

import com.gitlab.servertoolsbot.util.permissionmanager.configuration.PermissionStorageType;
import com.gitlab.servertoolsbot.util.permissionmanager.util.TristateUtils;
import com.gitlab.servertoolsbot.util.permissionmanager.util.database.DatabaseUtil;
import com.gitlab.servertoolsbot.util.permissionmanager.util.json.GetKey;
import com.gitlab.servertoolsbot.util.permissionmanager.util.json.GetObject;
import com.gitlab.servertoolsbot.util.permissionmanager.util.json.Storeback;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class StorageManager {
    private static PermissionManager manager = PermissionManager.getInstance();
    private static File dir = manager.getStorageType().getDir();

    public static void saveGroup(Group group) {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            Json.JsonGroup.save(group);
            return;
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            MariaDB.MariaGroup.save(group);
            return;
        }
        throwError();
    }
    public static void deleteGroup(Group group) {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            Json.JsonGroup.delete(group);
            return;
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            MariaDB.MariaGroup.delete(group);
            return;
        }
        throwError();
    }
    public static Group getGroup(String name) {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            return Json.JsonGroup.get(name);
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            return MariaDB.MariaGroup.get(name);
        }
        throwError();
        throw new IllegalStateException("This is an error, please contact the developers");
    }
    public static List<Group> getAllGroups() {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            return Json.JsonGroup.getAllGroups();
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            return MariaDB.MariaGroup.getAllGroups();
        }
        throwError();
        throw new IllegalStateException("This is an error, please contact the developers");
    }

    public static void saveUser(User user) {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            Json.JsonUser.save(user);
            return;
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            MariaDB.MariaUser.save(user);
            return;
        }
        throwError();
        throw new IllegalStateException("This is an error, please contact the developers");
    }
    public static void deleteUser(User user) {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            Json.JsonUser.delete(user);
            return;
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            MariaDB.MariaUser.delete(user);
            return;
        }
        throwError();
        throw new IllegalStateException("This is an error, please contact the developers");
    }
    public static User getUser(String name) {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            return Json.JsonUser.get(name);
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            return MariaDB.MariaUser.get(name);
        }
        throwError();
        throw new IllegalStateException("This is an error, please contact the developers");
    }
    public static List<User> getAllUsers() {
        if (manager.getStorageType() == PermissionStorageType.JSON) {
            checkDirs();
            return Json.JsonUser.getAllUsers();
        }
        if (manager.getStorageType() == PermissionStorageType.MARIADB) {
            return MariaDB.MariaUser.getAllUsers();
        }
        throwError();
        throw new IllegalStateException("This is an error, please contact the developers");
    }

    private static class Json {
        static class JsonPermissionObject {
            static JsonObject getAsJson(PermissionObject object) {
                JsonObject obj = new JsonObject();
                obj.addProperty("name", object.getName());

                JsonArray permissionArray = new JsonArray();
                object.getPermissions().forEach(permission -> {
                    JsonObject jsonPerm = new JsonObject();
                    jsonPerm.addProperty("name", permission.getName());
                    jsonPerm.addProperty("value", TristateUtils.asBoolean(permission.getValue()));
                    permissionArray.add(jsonPerm);
                });

                JsonArray parentArray = new JsonArray();
                object.getRawParents().forEach(parent -> parentArray.add(parent.getName()));

               obj.add("permissions", permissionArray);
               obj.add("parents", parentArray);

               return obj;
            }

            static PermissionObject get(File file, String name) {
                JsonObject jsonObj = GetObject.get(file).get(name).getAsJsonObject();
                PermissionObject permObj = new PermissionObject();
                permObj.setName(jsonObj.get("name").getAsString());
                jsonObj.get("permissions").getAsJsonArray().forEach(obj -> permObj.addPermission(obj.getAsJsonObject().get("name").getAsString(), TristateUtils.toTristate(obj.getAsJsonObject().get("value").getAsBoolean())));
                jsonObj.get("parents").getAsJsonArray().forEach(parent -> permObj.addParent(manager.getGroupManager().getGroup(parent.getAsString())));
                return permObj;
            }
        }

        static class JsonGroup {
            static void save(Group group) {
                JsonObject input = GetObject.get(StorageManager.getGroupFile());
                if (GetKey.get(input, group.getName()) != null) delete(group);
                JsonObject groupObj = Json.JsonPermissionObject.getAsJson(group);
                groupObj.addProperty("weight", group.getWeight());
                input.add(group.getName(), groupObj);
                Storeback.toFile(StorageManager.getGroupFile(), input);
            }

            static void delete(Group group) {
                JsonObject obj = GetObject.get(StorageManager.getGroupFile());
                obj.keySet().forEach(key -> {
                    if (group.equals(get(key))) obj.remove(key);
                });
                Storeback.toFile(StorageManager.getGroupFile(), obj);
            }

            static Group get(String name) {
                Group group = Json.JsonPermissionObject.get(StorageManager.getGroupFile(), name).toGroup();
                group.setWeight(GetObject.get(StorageManager.getGroupFile()).get(name).getAsJsonObject().get("weight").getAsInt());
                return group;
            }
            static List<Group> getAllGroups() {
                JsonObject obj = GetObject.get(StorageManager.getGroupFile());
                List<Group> toReturn = new ArrayList<>();
                obj.keySet().forEach(key -> {
                    toReturn.add(get(key));
                });
                return toReturn;
            }
        }

        static class JsonUser {
            static void save(User user) {
                JsonObject input = GetObject.get(StorageManager.getUserFile());
                if (GetKey.get(input, user.getName()) != null) delete(user);
                input.add(user.getName(), Json.JsonPermissionObject.getAsJson(user));
                Storeback.toFile(StorageManager.getUserFile(), input);
            }

            static void delete(User user) {
                JsonObject obj = GetObject.get(StorageManager.getUserFile());
                obj.keySet().forEach(key -> {
                    if (user.equals(get(key))) obj.remove(key);
                });
                Storeback.toFile(StorageManager.getUserFile(), obj);
            }

            static User get(String name) {
                return JsonPermissionObject.get(StorageManager.getUserFile(), name).toUser();
            }
            static List<User> getAllUsers() {
                JsonObject obj = GetObject.get(StorageManager.getUserFile());
                List<User> toReturn = new ArrayList<>();
                obj.keySet().forEach(key -> {
                    toReturn.add(get(key));
                });
                return toReturn;
            }
        }
    }
    private static class MariaDB {
        private static DatabaseUtil dbUtil = new DatabaseUtil(manager.getConnection());
        private static PermissionStorageType type = manager.getStorageType();

        static class MariaPermissionObject {
            static PermissionObject get(String table, String parentTable, String permissionTable, String name) {
                PermissionObject permObj = getObject(table, name);
                getPermissions(permissionTable, name).forEach(permObj::addPermission);
                getParents(parentTable, name).forEach(permObj::addParent);
                return permObj;
            }

            static PermissionObject getObject(String table, String name) {
                PermissionObject permObj = new PermissionObject();
                ResultSet result = dbUtil.getResultFromQuery("SELECT id, name FROM " + table + " WHERE id='" + name + "';");
                try {
                    while (result.next()) permObj.setName(result.getString("id"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return permObj;
            }

            static List<Permission> getPermissions(String permissionTable, String name) {
                List<Permission> permissions = new ArrayList<>();
                ResultSet result = dbUtil.getResultFromQuery("SELECT name, value FROM " + permissionTable + " WHERE id='" + name + "';");
                try {
                    while (result.next()) permissions.add(new Permission(result.getString("name"), TristateUtils.toTristate(Boolean.parseBoolean(result.getString("value")))));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return permissions;
            }
            static List<Group> getParents(String parentTable, String name) {
                List<Group> parents = new ArrayList<>();
                ResultSet result = dbUtil.getResultFromQuery("SELECT parent FROM " + parentTable + " WHERE id='" + name + "';");
                try {
                    while (result.next()) parents.add(manager.getGroupManager().getGroup(result.getString("parent")));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return parents;
            }
        }

        static class MariaGroup {
            static void save(Group group) {
                ResultSet test = dbUtil.getResultFromQuery("SELECT id FROM " + type.getGroupTable() + " WHERE id='" + group.getName() + "';");
                try {
                    while (test.next()) if (!test.getString("id").equals("")) delete(group);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dbUtil.getResultFromQuery("INSERT INTO " + type.getGroupTable() + " (id, name, weight) VALUES ( '" + group.getName() + "', '" + group.getName() + "', '" + group.getWeight() + "' );");
                group.getRawParents().forEach(parent -> dbUtil.getResultFromQuery("INSERT INTO " + type.getGroupParentTable() + "(id, parent) VALUES ( '" + group.getName() + "', '" + parent.getName() + "');"));
                group.getRawPermissions().forEach(permission -> dbUtil.getResultFromQuery("INSERT INTO " + type.getGroupPermissionTable() + "(id, name, value) VALUES ( '" + group.getName() + "', '" + permission.getName() + "', '" + TristateUtils.asBoolean(permission.getValue()) +"' );"));
            }

            static void delete(Group group) {
                dbUtil.getResultFromQuery("DELETE FROM " + type.getGroupPermissionTable() + " WHERE id='" + group.getName() + "';");
                dbUtil.getResultFromQuery("DELETE FROM " + type.getGroupParentTable() + " WHERE id='" + group.getName() + "';");
                dbUtil.getResultFromQuery("DELETE FROM " + type.getGroupTable() + " WHERE id='" + group.getName() + "';");
            }

            static Group get(String name) {
                Group group = MariaPermissionObject.get(type.getGroupTable(), type.getGroupParentTable(), type.getGroupPermissionTable(), name).toGroup();
                ResultSet weightSet = dbUtil.getResultFromQuery("SELECT weight FROM " + type.getGroupTable() + " WHERE id='" + name + "';");
                int weight = 0;
                try {
                    while (weightSet.next()) weight = Integer.parseInt(weightSet.getString("weight"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                group.setWeight(weight);
                return group;
            }
            static List<Group> getAllGroups() {
                ResultSet it = dbUtil.getResultFromQuery("SELECT DISTINCT id FROM " + type.getGroupTable() + ";");
                List<Group> toReturn = new ArrayList<>();
                try {
                    while (it.next()) toReturn.add(get(it.getString("id")));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return toReturn;
            }
        }

        static class MariaUser {
            static void save(User user) {
                ResultSet test = dbUtil.getResultFromQuery("SELECT id FROM " + type.getUserTable() + " WHERE id='" + user.getName() + "';");
                try {
                    while (test.next()) if (!test.getString("id").equals("")) delete(user);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dbUtil.getResultFromQuery("INSERT INTO " + type.getUserTable() + " (name, id) VALUES ( '" + user.getName() + "', '" + user.getName() + "' );");
                user.getRawParents().forEach(parent -> dbUtil.getResultFromQuery("INSERT INTO " + type.getUserParentTable() + "(id, parent) VALUES ( '" + user.getName() + "', '" + parent.getName() + "');"));
                user.getRawPermissions().forEach(permission -> dbUtil.getResultFromQuery("INSERT INTO " + type.getUserPermissionTable() + "(id, name, value) VALUES ( '" + user.getName() + "', '" + permission.getName() + "', '" + TristateUtils.asBoolean(permission.getValue()) +"' );"));
            }

            static void delete(User user) {
                dbUtil.getResultFromQuery("DELETE FROM " + type.getUserTable() + " WHERE id='" + user.getName() + "';");
                dbUtil.getResultFromQuery("DELETE FROM " + type.getUserPermissionTable() + " WHERE id='" + user.getName() + "';");
                dbUtil.getResultFromQuery("DELETE FROM " + type.getUserParentTable() + " WHERE id='" + user.getName() + "';");
            }

            static User get(String name) {
                return MariaPermissionObject.get(type.getUserTable(), type.getUserParentTable(), type.getUserPermissionTable(), name).toUser();
            }
            static List<User> getAllUsers() {
                ResultSet it = dbUtil.getResultFromQuery("SELECT DISTINCT id FROM " + type.getUserTable() + ";");
                List<User> toReturn = new ArrayList<>();
                try {
                    while (it.next()) toReturn.add(get(it.getString("id")));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return toReturn;
            }
        }
    }

    private static void checkDirs() {
        File dir = StorageManager.getDir();
        dir.mkdirs();
        JsonObject obj = new JsonObject();
        List<File> toCreate = new ArrayList<>();
        if (!StorageManager.getUserFile().exists()) toCreate.add(StorageManager.getUserFile());
        if (!StorageManager.getGroupFile().exists()) toCreate.add(StorageManager.getGroupFile());
        toCreate.forEach(file -> {
            try (FileWriter writer = new FileWriter(file)) {
                writer.append(new GsonBuilder().setPrettyPrinting().create().toJson(obj));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    private static void throwError() {
        throw new IllegalStateException("Couldn't properly use the object for this StorageType, please contact the developer");
    }

    private static File getDir() {
        return dir;
    }
    private static File getGroupFile() {
        return new File(getDir() + "/groups.json");
    }
    private static File getUserFile() {
        return new File(getDir() + "/users.json");
    }
}
